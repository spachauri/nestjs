import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { JwtauthService } from '../jwtauth/jwtauth.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private readonly ref: Reflector,
    private readonly jwts: JwtService,
  ) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const ctx = context.switchToHttp();
    const req = ctx.getRequest();
    //const user = this.jwtauth.getUserName(req);
    const payload = req.headers['authorization'].replace('Bearer ', '');
    const userload = this.jwts.decode(payload, { complete: true });
    const user = userload['payload']['user'];
    const allowedUser = this.ref.get<String[]>('user', context.getHandler());
    if (user == allowedUser) {
      return true;
    }
    return false;
  }
}
