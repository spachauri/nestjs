import { HttpException, ExceptionFilter } from '@nestjs/common';
import { Catch, ArgumentsHost } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class AddException implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    // response.status(status).json({
    //         statusCode: status,
    //         timestamp: new Date().toISOString(),
    //         path: request.url,
    //         message:'Sorry you caught in exception block',
    //         reason:'Not so specific reasoning'
    //       });
    console.log('Inside Exception filter');
    response
      .status(status)
      .send('Hello there we are in exception block , go back try again!');
  }
}
