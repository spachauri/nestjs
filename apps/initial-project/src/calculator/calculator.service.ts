import { Injectable, Scope } from '@nestjs/common';

@Injectable({ scope: Scope.REQUEST })
export class CalculatorService {
  val: string = 'Shrasti';
  add(a: number, b: number): number {
    console.log(a + b);
    return a + b;
  }

  sub(a: number, b: number): number {
    console.log(a - b);
    return a - b;
  }

  mul(a: number, b: number): number {
    console.log(a * b);
    return a * b;
  }

  div(a: number, b: number): number {
    console.log(a / b);
    return a / b;
  }
}
