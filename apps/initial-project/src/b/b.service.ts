import { Injectable, Inject } from '@nestjs/common';
import { AService } from '../a/a.service';
import { forwardRef } from '@nestjs/common';

@Injectable()
export class BService {
  constructor(
    @Inject(forwardRef(() => AService)) private readonly a_obj: AService,
  ) {}

  getVal(): number {
    return 5;
  }
}
