import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Query,
  Inject,
  UseFilters,
  HttpStatus,
  Req,
  UseGuards,
  SetMetadata,
} from '@nestjs/common';
import { AppService } from './app.service';
import { User } from './user.dto';
import { operand } from './operand.dto';
import { CalculatorService } from './calculator/calculator.service';
import { AddException } from './ExceptionFilter';
import { HttpException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AService } from './a/a.service';
import { AuthGuardService } from './auth-guard/auth-guard.service';
import { JwtauthService } from './jwtauth/jwtauth.service';

@Controller()
export class AppController {
  constructor(
    private readonly a: AService,
    private readonly jwtService: JwtService,
    @Inject('b') private readonly num: CalculatorService,
    private readonly appService: AppService,
    private readonly calculatorService: CalculatorService,
    private readonly jwtauth: JwtauthService,
  ) {}

  @Get('add')
  getResult(@Body() op: operand): void {
    this.calculatorService.add(parseInt(op.a + ''), parseInt(op.b + ''));
  }

  //jwt decoding
  // @Get()
  // getHello(@Req() req: Request): any {
  //   if (req.headers['authorization']) {
  //     const jwt = req.headers['authorization'].replace('Bearer ', '');
  //     console.log(`jwt is ${jwt}`);
  //     const json = this.jwtService.decode(jwt, { complete: true });
  //     console.log(json);
  //   }
  @SetMetadata('user', ['shrasti'])
  @UseGuards(AuthGuardService)
  @Get()
  getHello(@Req() req: Request): any {
    // if (req.headers['authorization']) {
    //   const jwt = req.headers['authorization'].replace('Bearer ', '');
    //   console.log(`jwt is ${jwt}`);
    //   const json = this.jwtService.decode(jwt, { complete: true });
    //   console.log(json);
    // }
    return this.appService.getHello();
  }
  //to create a new object dto object from parameter coming from client side
  @Post()
  getDetails(@Body() details: User): void {
    console.log(details);
  }

  @Post('change')
  getCalci() {
    this.calculatorService.val = 'Shri';
    console.log(this.calculatorService.val);
  }

  @Get('value')
  getValue(): void {
    throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    //this.num.add(1,2);
  }

  @Get('checkCircularDependency')
  getValueFromService(): any {
    console.log(this.a.getVal()); // this is case of circular dependency A->B->A
  }
}
