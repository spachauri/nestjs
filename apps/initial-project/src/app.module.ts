import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsService } from './cats/cats.service';
import { CalculatorService } from './calculator/calculator.service';
import { NestModule } from '@nestjs/common';
import { MiddlewareConsumer } from '@nestjs/common';
import { FirstMiddleware } from './Firstmiddleware';
import { AddException } from './ExceptionFilter';
import { APP_FILTER } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { AService } from './a/a.service';
import { BService } from './b/b.service';
import { SecondMiddleWare } from './SecondMiddleWare';
import { AuthGuardService } from './auth-guard/auth-guard.service';
import { JwtauthService } from './jwtauth/jwtauth.service';


@Module({
  imports: [JwtModule.register({ secret: 'secret' })],
  controllers: [AppController],
  providers: [
    {
      provide: 'b',
      useClass: CalculatorService,
    },
    AppService,
    CatsService,
    CalculatorService,
    { provide: APP_FILTER, useClass: AddException},AuthGuardService, JwtauthService,
    AService,
    BService,
  ],
  exports: [CalculatorService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(FirstMiddleware, SecondMiddleWare).forRoutes('*/');
  }
}
