import { Inject, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { REQUEST } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class JwtauthService {
  constructor(private readonly jwts: JwtService) {}

  getUserName(@Inject(REQUEST) req: Request): any {
    const payload = req.headers['authorization'].replace('bearer ', '');
    const user = this.jwts.decode(payload, { complete: true });
    console.log('user ' + user);
    return user;
  }
}
