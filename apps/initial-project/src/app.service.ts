import { Injectable } from '@nestjs/common';
import { User } from './user.dto';
import { Controller, Get, Post, Body } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
