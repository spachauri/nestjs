import { Inject, Injectable } from '@nestjs/common';
import { BService } from '../b/b.service';
import { forwardRef } from '@nestjs/common';

@Injectable()
export class AService {
  constructor(
    @Inject(forwardRef(() => BService)) private readonly b_obj: BService,
  ) {}

  getVal(): number {
    return 6;
  }
}
