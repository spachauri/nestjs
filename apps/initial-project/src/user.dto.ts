import {
  IsAlpha,
  IsNotEmpty,
  IsString,
  IsNumber,
  IsOptional,
} from 'class-validator';
export class User {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  department: string;

  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  year: number;

  color: string;
}
