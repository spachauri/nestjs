import { Controller, Get , Post } from '@nestjs/common';
import { SecondProjectService } from './second-project.service';
import { CalculatorService } from 'apps/initial-project/src/calculator/calculator.service';

@Controller()
export class SecondProjectController {
  constructor(private readonly secondProjectService: SecondProjectService , private readonly calci:CalculatorService) {}

  @Get()
  getHello(): string {
    return this.secondProjectService.getHello();
  }

  @Post()
  getCalci(){
    console.log(this.calci.val);
    this.calci.val='Pachauri';
    console.log('after second request')
    console.log(this.calci.val);
  }

}
