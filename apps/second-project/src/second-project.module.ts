import { Module } from '@nestjs/common';
import { SecondProjectController } from './second-project.controller';
import { SecondProjectService } from './second-project.service';
import { AppModule } from 'apps/initial-project/src/app.module';

@Module({
  imports: [AppModule],
  controllers: [SecondProjectController],
  providers: [SecondProjectService],
  
})
export class SecondProjectModule {}
